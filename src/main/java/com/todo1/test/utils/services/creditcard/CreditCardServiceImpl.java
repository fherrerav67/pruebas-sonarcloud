/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2019.
 *  
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.test.utils.services.creditcard;

/**
 * Implementacion servicio de tarjetas de credito
 * 
 * @author fherrera
 *
 * @since Dec 11, 2019
 *
 */
public class CreditCardServiceImpl implements CreditCardService {

	/** 
	 * {@inheritDoc}
	 *
	 * @see com.todo1.test.utils.services.creditcard.CreditCardService#validate(java.lang.String)
	 */
	@Override
	public boolean validate(String cardNumber) {
		if ((cardNumber != null) && (cardNumber.length() >= 13) && (cardNumber.length() <= 19)) {
			return checkLuhn(cardNumber);
		}
		return false;
	}

	/**
	 * Modulo 10
	 * 
	 * @param cardNumber tarjeta
	 * @return
	 */
	private boolean checkLuhn(String cardNumber) {
		int nDigits = cardNumber.length();

		int nSum = 0;
		boolean isSecond = false;
		for (int i = nDigits - 1; i >= 0; i--) {
			int d = cardNumber.charAt(i) - '0';
			if (isSecond) {
				d = d * 2;
			}
			// We add two digits to handle
			// cases that make two digits
			// after doubling
			nSum += d / 10;
			nSum += d % 10;

			isSecond = !isSecond;
		}
		return (nSum % 10 == 0);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see com.todo1.test.utils.services.creditcard.CreditCardService#getCardBrand(java.lang.String)
	 */
	@Override
	public CardBrand getCardBrand(String cardNumber) {
		return CardBrand.getCardBrand(cardNumber);
	}

}
