/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2020.
 *  
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.test.utils.services.watchservice;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fherrera
 *
 * @since May 1, 2020
 * 
 *        Ver: https://howtodoinjava.com/java8/java-8-watchservice-api-tutorial/
 *
 */
public class WatchServiceDemo {

	private static final Logger logger = LoggerFactory.getLogger(WatchServiceDemo.class);

	private WatchService watchService;

	private static final String[] dirs = { "C:/Temp/monitor/A", "C:/Temp/monitor/B" };

	private final Map<WatchKey, String> keys;

	public WatchServiceDemo() throws IOException {
		watchService = FileSystems.getDefault().newWatchService();
		keys = new HashMap<>();
	}

	/**
	 * Setup dirs to monitor
	 * 
	 * @throws IOException
	 */
	public void setup() throws IOException {
		for (String dirToRegister : dirs) {
			logger.info("Path registered: [{}]", dirToRegister);
			Path path = Paths.get(dirToRegister);
			WatchKey key = path.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
			keys.put(key, dirToRegister);
		}
	}

	/**
	 * Monitor dirs
	 * 
	 * @throws InterruptedException
	 */
	public void monitor() throws InterruptedException {
		WatchKey key;
		while ((key = watchService.take()) != null) {
			for (WatchEvent<?> event : key.pollEvents()) {
				Path name = (Path) event.context();
				File file = getFile(keys.get(key), name);
				logger.info("Event kind: [{}] - File affected: [{}]", event.kind(), file.getAbsoluteFile());
			}
			key.reset();
		}
	}

	/**
	 * @param path
	 * @param string
	 * @return
	 */
	private File getFile(String dir, Path name) {
		return new File(dir + "/" + name);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			WatchServiceDemo watchService = new WatchServiceDemo();
			logger.info("Init");
			watchService.setup();
			logger.info("Monitor Up");
			watchService.monitor();
		} catch (IOException | InterruptedException e) {
			logger.error("Problemas ejecutando monitor!", e);
		}

	}

}
