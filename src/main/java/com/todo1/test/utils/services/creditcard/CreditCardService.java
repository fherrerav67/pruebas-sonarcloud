/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2019.
 *  
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.test.utils.services.creditcard;

/**
 * Servicio para validar tarjetas de credito
 * 
 * @author fherrera
 *
 * @since Dec 11, 2019
 *
 */
public interface CreditCardService {

	/**
	 * Valida si un numero de tarjeta es valido
	 * 
	 * @param cardNumber numero de tarjeta
	 * @return <i>true</i> si el numero es valido
	 */
	public boolean validate(String cardNumber);

	/**
	 * Retorna la franquicia de la tarjeta
	 * 
	 * @param cardNumber tarjeta
	 * @return franquicia
	 */
	public CardBrand getCardBrand(String cardNumber);


}
