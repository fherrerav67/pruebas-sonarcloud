/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2019.
 *  
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.test.utils.services.creditcard;

/**
 * @author fherrera
 *
 * @since Dec 13, 2019
 *
 */
public enum CardBrand {

	// DISC /^(?:6011\d{12})|(?:65\d{14})$/
	// ^6(?:011|5[0-9]{2})[0-9]{12}$

	VISA("^4[0-9]{12}(?:[0-9]{3})?$", "VISA"), MASTERCARD("^5[1-5][0-9]{14}$", "MASTER"),
	AMEX("^3[47][0-9]{13}$", "AMEX"), DINERS("^3(?:0[0-5]|[68][0-9])[0-9]{11}$", "Diners"),
	DISCOVER("^6(?:011|5\\d\\d)(| |-)(?:\\d{4}\\1){2}\\d{4}$", "DISCOVER"),
	JCB("^(?:2131|1800|35\\d{3})\\d{11}$", "JCB");

	private String regex;
	private String issuerName;

	/**
	 * @param regex
	 * @param issuerName
	 */
	CardBrand(String regex, String issuerName) {
		this.regex = regex;
		this.issuerName = issuerName;
	}

	public boolean matches(String card) {
		return card.matches(this.regex);
	}

	public String getIssuerName() {
		return this.issuerName;
	}

	/**
	 * get an enum from a card number
	 * 
	 * @param card
	 * @return
	 */
	public static CardBrand getCardBrand(String card) {
		for (CardBrand cc : CardBrand.values()) {
			if (cc.matches(card)) {
				return cc;
			}
		}
		return null;
	}

	/**
	 * get an enum from an issuerName
	 * 
	 * @param issuerName
	 * @return
	 */
	public static CardBrand getCardBrandByIssuerName(String issuerName) {
		for (CardBrand cc : CardBrand.values()) {
			if (cc.getIssuerName().equalsIgnoreCase(issuerName)) {
				return cc;
			}
		}
		return null;
	}
}
