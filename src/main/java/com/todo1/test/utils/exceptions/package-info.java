/**
 * This package includes Exceptions. It can contain sub-packages for different categories.
 * 
 * com.todo1.[appl].[module].exceptions
 * 
 * @author TODO1 Services
 *
 */
package com.todo1.test.utils.exceptions;