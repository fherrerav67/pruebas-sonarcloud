/**
 * This package includes annotations classes. It can contain sub-packages for different categories.
 *
 * com.todo1.[appl].[module].annotations
 * 
 * @author TODO1 Services
 *
 */
package com.todo1.test.utils.annotations;
