/**
 * This package includes the business beans. It can contain sub-packages for different categories. 
 * 
 * com.todo1.[appl].[module].beans
 * 
 * @author TODO1 Services
 *
 */
package com.todo1.test.utils.beans;