/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2019.
 *  
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.test.utils.services.creditcard;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * 
 * @author fherrera
 *
 * @since Dec 11, 2019
 *
 */
@DisplayName("CreditCardService")
class CreditCardServiceImplTest {

	private CreditCardServiceImpl creditCardService;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		creditCardService = new CreditCardServiceImpl();
	}

	/**
	 * Test method for
	 * {@link com.todo1.test.utils.services.creditcard.CreditCardServiceImpl#validate(java.lang.String)}.
	 */
	@ParameterizedTest(name = "Test #{index} => Card=[{0}]")
	@MethodSource("validCardProvider")
	@DisplayName("Valid cards")
	void testValidate(String cardNumber) {
		assertTrue(creditCardService.validate(cardNumber));
	}

	static Stream<Arguments> validCardProvider() {
		return Stream.of(Arguments.of("4976921701895723", "VISA"), Arguments.of("4023440185356170", "VISA"),
				Arguments.of("5569722107666969", "MASTER"), Arguments.of("5393343930811607", "MASTER"),
				Arguments.of("6011725786081311", "DISCOVERY"), Arguments.of("6011954957266556", "DISCOVERY"),
				Arguments.of("370354834741558", "AMEX"), Arguments.of("340797317258911", "AMEX"),
				Arguments.of("344085806231359", "AMEX"), Arguments.of("3548979135448833", "JCB"),
				Arguments.of("3589960696096146", "JCB"));
	}

	@ParameterizedTest(name = "Test #{index} => Card=[{0}]")
	@MethodSource("invalidCardProvider")
	@DisplayName("Invalid cards")
	void testValidateFalse(String cardNumber) {
		assertFalse(creditCardService.validate(cardNumber));
	}

	static Stream<Arguments> invalidCardProvider() {
		String nulo = null;
		return Stream.of(Arguments.of("4976921701895720"), Arguments.of("4023440185703"), Arguments.of("55696969"),
				Arguments.of("533393343930811607"), Arguments.of("6011725783336081311"),
				Arguments.of("60133331954957266556"), Arguments.of("370354334741558"), Arguments.of("340792317258911"),
				Arguments.of(nulo));
	}

	@ParameterizedTest(name = "Test #{index} => Card=[{0}]")
	@MethodSource("validCardProvider")
	@DisplayName("Get Card Brand")
	void testGetCardBrand(String cardNumber, String brand) {
		CardBrand cardBrand = CardBrand.getCardBrandByIssuerName(brand);
		assertEquals(cardBrand, creditCardService.getCardBrand(cardNumber));
	}

}
