#!/usr/bin/env groovy

pipeline {

	agent any
	tools {
	    maven 'MAVEN 3.6.x'
	    jdk 'OpenJDK 1.8 - Red Hat'
	}

	stages{
	
	 	stage ("Initialize") {
            steps {
            	sh label: '', 
            	script: '''
            			echo "PATH = ${PATH}"
		                echo "M2_HOME = ${M2_HOME}"
		                echo "WORKSPACE = ${WORKSPACE}"
		                java -version
		                mvn -version
		                '''
			}
        }
        
        stage("SCM"){
		    steps{
				checkout([$class: 'GitSCM', 
					branches: [[name: '*/master']], 
					doGenerateSubmoduleConfigurations: false, 
					extensions: [], 
					gitTool: 'Default', 
					submoduleCfg: [], 
					userRemoteConfigs: [[credentialsId: 'c08d9f0e-97ff-4bdb-a56f-c287deafa881', 
					url: 'git@bitbucket.org:todo1services/demo-utils.git']]])	        
		    }
	    }
  
	    stage ("Build") {
            steps {
            	withMaven(maven: 'MAVEN 3.6.x', 
            		mavenLocalRepo:'$WORKSPACE/.repository') {
            		sh 'mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent package -Dmaven.test.failure.ignore=true'    
            	}
            }
        }
        
        stage ("SonarQube") {
            steps {
	            withSonarQubeEnv('SONAR CDV4'){
	            	withMaven(maven: 'MAVEN 3.6.x', 
	            		mavenLocalRepo:'$WORKSPACE/.repository') {
	            		sh 'mvn org.codehaus.mojo:sonar-maven-plugin:LATEST:sonar'    
	            	}
	            }
            }
        }
        
        stage("SonarQube Quality Gate") { 
        	steps {
	        	script {
	        		sleep(60)
			        timeout(time: 5, unit: 'MINUTES') { 
		           		def qg = waitForQualityGate() 
		           		if (qg.status != 'OK') {
		             		error "Pipeline aborted due to quality gate failure: ${qg.status}"
		           		}
        			}
				}
        	}
    	}
        
        stage ("Publish to Nexus") {
            steps {
            	withMaven(maven: 'MAVEN 3.6.x', 
            		mavenLocalRepo:'$WORKSPACE/.repository') {
            		sh 'mvn deploy -DskipTests=true'    
            	}
            }
        }
	}
	post {
        always {
            echo 'Proceso culmino'
            cleanWs(patterns: [[pattern: '**/target/*', type: 'INCLUDE'], 
            				   [pattern: '**/.repository/com/todo1/*', type: 'INCLUDE']])
        }
        success {
            echo 'Finalizado con exito! :)'
        }
        unstable {
            mail bcc: '', body: 'Resultado del QG: unstable', cc: '', from: 'continuum@todo1.net', replyTo: '', subject: 'Evaluacion Quality Gate Demo', to: 'fherrera@todo1.com'
        }
        failure {
            mail bcc: '', body: 'Resultado del QG: failure' , cc: '', from: 'continuum@todo1.net', replyTo: '', subject: 'Evaluacion Quality Gate Demo', to: 'fherrera@todo1.com'
        }
        changed {
            echo 'Se encontraron cambios...'
        }
    }
}
